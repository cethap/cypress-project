Feature: Book travel to custom planet in Space & Beyond
  Scenario: Book travel to Sant Cugat Del Valles with login
    Given I open page 
    When Login with custom user
    Then Departing input is visible

  Scenario: Book travel to Sant Cugat Del Valles with login and logout
    Given I open page 
    When Login with custom user
    Then Departing input is visible
    But I logout