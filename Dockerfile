FROM cypress/browsers:node14.16.0-chrome90-ff88
RUN mkdir /automation
WORKDIR /automation
COPY . .
RUN mkdir junit_report && chmod -R 777 junit_report
RUN rm -rf .git
RUN npm install
#--env TAGS='@e2e-test'
ENTRYPOINT [ "npm", "run", "test:remote" ]