import { Given, When, And } from 'cypress-cucumber-preprocessor/steps';

const url = 'http://demo.testim.io'

before(() => {
  cy.log(
    "Common global log"
  );
});

Given(`I open page`, () => {
  cy.visit(url)
})

Given(`Login with custom user`, () => {
  cy.findByText(/LOG IN/i).should('be.visible').click()
  cy.findByText(/Username/i).siblings('input').should('be.visible').click().type('John')
  cy.findByText(/Password/i).siblings('input').should('be.visible').click().type('SecretPass')
  cy.get('nav').within(()=>{
    cy.findByText(/LOG IN/i).click()
  })
})

When(`Schedule travel date`, () => {
  cy.findByText(/Departing/i).siblings('input').should('be.visible').click()

  var date = new Date();
  const currentDay = date.getDate()
  date.setDate(date.getDate() + 2)
  const nextDay = date.getDate()
  cy.get('[data-react-toolbox="calendar"]').within(()=>{
    if(nextDay<currentDay){
      cy.get('#right').click()
      cy.findByText(nextDay).should('be.visible').click()
    }else{
      cy.findByText(nextDay).should('be.visible').click()
    }
  })
  cy.get('nav').within(()=>{
    cy.findByText(/OK/i).click()
  })
})

And(/^Select (.*) adults$/, (numberAdults) => {
  cy.findByDisplayValue('Adults (18+)').click()
  cy.findByText(/Adults \(18\+\)/i).parent().within(()=>{
    cy.findAllByText(numberAdults).should('be.visible').click({force:true})
  })
});

And(/^Select (.*) children$/, (numberChildren) => {
  cy.findByDisplayValue('Children (0-7)').click()
  cy.findByText(/Children \(0\-7\)/i).parent().within(()=>{
    cy.findAllByText(numberChildren).should('be.visible').click({force:true})
  })
});

And(/^Select a (.*) planet$/, (planetTravel) => {
  cy.findByDisplayValue('Launch').click()
  cy.findAllByText(planetTravel).should('be.visible').eq(0).dblclick()
  cy.findAllByText(/BOOK/i).should('be.visible').click()
});