import { When, Then } from 'cypress-cucumber-preprocessor/steps';

When(`Login with custom user`, () => {
  cy.findByText(/LOG IN/i).should('be.visible').click()
  cy.findByText(/Username/i).siblings('input').should('be.visible').click().type('John')
  cy.findByText(/Password/i).siblings('input').should('be.visible').click().type('SecretPass')
  cy.get('nav').within(()=>{
    cy.findByText(/LOG IN/i).click()
  })
})

Then(`Departing input is visible`, () => {
  cy.findByText(/Departing/i).siblings('input').should('be.visible')
})