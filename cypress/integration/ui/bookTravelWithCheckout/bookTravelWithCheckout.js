import { Given, When, And, Then } from 'cypress-cucumber-preprocessor/steps';

When(`Get checkout form`, () => {
  cy.findAllByText(/BOOKED/i).should('be.visible')
  cy.findAllByText(/CHECKOUT/i).should('be.visible')
})

And(`fill the form with default data`, () => {
  cy.findByText(/Name/i).siblings('input').should('be.visible').click().type('Manizales')
  cy.findByText(/Email Address/i).siblings('input').should('be.visible').click().type('caldas@colomvia.com')
  cy.findByText(/Social Security Number/i).siblings('input').should('be.visible').click().type('122-12-1222')
  cy.findByText(/Phone Number/i).siblings('input').should('be.visible').click().type('3333')
})
