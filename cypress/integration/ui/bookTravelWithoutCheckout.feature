Feature: Book travel to custom planet in Space & Beyond
  Scenario: Book travel to Sant Cugat Del Valles with login without checkout
    Given I open page 
      And Login with custom user
    When Schedule travel date
      And Select 1 adults
      And Select 1 children
      And Select a Sant Cugat Del Valles planet
    Then Get checkout form