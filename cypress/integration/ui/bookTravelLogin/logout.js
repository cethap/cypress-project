import { But } from 'cypress-cucumber-preprocessor/steps';

But(`I logout`, () => {
  cy.findByText(/HELLO, JOHN/i).should('be.visible').click()
  cy.findByText(/Log out/i).should('be.visible').click()
  cy.findByText(/LOG IN/i).should('be.visible').click()
  cy.findByText(/Username/i).siblings('input').should('be.visible')
})