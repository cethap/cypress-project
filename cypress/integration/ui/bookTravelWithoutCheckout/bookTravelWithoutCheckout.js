import { Then } from 'cypress-cucumber-preprocessor/steps';

Then(`Get checkout form`, () => {
    cy.findAllByText(/BOOKED/i).should('be.visible')
    cy.findAllByText(/CHECKOUT/i).should('be.visible')
})